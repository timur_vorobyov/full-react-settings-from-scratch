import webpack from 'webpack';
import { buildPlugins } from './buildPlugins';
import { buildResolvers } from './buildResolvers';
import { BuildOptions } from './types/config';
import { buildDevServer } from './buildDevServer';
import { buildLoaders } from './buildLoaders';

export function buildWebpackConfig(
    options: BuildOptions,
): webpack.Configuration {
    const { paths, mode, isDev } = options;

    return {
        mode,
        entry: paths.entry,
        output: {
            filename: '[name].[contenthash].js',
            path: paths.build,
            clean: true,
        },
        plugins: buildPlugins(options),
        module: {
            // rules здесь мы конфигурируем так называемые LOADER они предназначены для
            // того чтобы обрабатывать файлы которые выходят за рамки js
            rules: buildLoaders(options),
        },
        // resolve extensions позволяет не указывать расширение при импорте
        resolve: buildResolvers(options),
        // показывает где и в каком файле произошла ошибка в случае когда
        // 3 файла собирается в один например
        // Когда webpack объединяет ваш исходный код, может быть сложно отследить ошибку
        // и предупреждения до их исходного местоположения. Например, если вы объединяете три
        // исходных файла (a.js, b.js u c.js) в один пакет (bundle.js) и один из исходных файлов
        // содержит ошибку, трассировка стека будет указывать на bundle.js. . Это не всегда полезно,
        // поскольку вы, вероятно, хотите точно знать, из какого исходного файла возникла ошибка.
        devtool: isDev ? 'inline-source-map' : undefined,
        devServer: isDev ? buildDevServer(options) : undefined,
    };
}
