import MiniCssExtractPlugin from 'mini-css-extract-plugin';

export function buildCssLoader(isDev: boolean) {
    return {
        test: /\.s[ac]ss$/i,
        use: [
        // Creates `style` from JS strings
        // Creates a css file per JS file wich requires CSS
            isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
            // Translate CSS into CommonJS
            {
                loader: 'css-loader',
                options: {
                    modules: {
                    // eslint-disable-next-line max-len
                        auto: (resPath: string) => Boolean(resPath.includes('.module.')),
                        localIdentName: isDev
                            ? '[path][name]__[local]--[hash:base64:5]'
                            : '[hash:base64:8]',
                    },
                },
            },
            // Compiles Sass to CSS
            'sass-loader',
        ],
    };
}
